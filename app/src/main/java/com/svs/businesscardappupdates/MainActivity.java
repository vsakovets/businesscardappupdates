package com.svs.businesscardappupdates;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.svs.businesscardappupdates.Dialog.Dialog;
import com.svs.businesscardappupdates.Dialog.ErrorDialog;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CAMERA_REQUEST = 0;
    private static final int PERMISSION_REQUEST_CODE = 123;
    Util util = new Util();
    Button btnCall, btnEmail, btnSave, btnLoad;
    boolean editable;
    MenuItem menuItemEdit, menuItemSave;
    EditText editName, editPhone, editEmail;
    String email, phone, name;
    Bitmap thumbnailBitmap;
    ImageView imgPhoto;
    private String nameFile = "dataPhoto.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCall = findViewById(R.id.btnCall);
        btnEmail = findViewById(R.id.btnEmail);
        editName = findViewById(R.id.etEditName);
        editPhone = findViewById(R.id.etEditPhone);
        editEmail = findViewById(R.id.etEditEmail);
        imgPhoto = findViewById(R.id.ivPhoto);
        btnLoad = findViewById(R.id.btnLoad);
        btnSave = findViewById(R.id.btnSave);

//при повороте экрана фото не пропадает
        if (savedInstanceState != null) {
            thumbnailBitmap = savedInstanceState.getParcelable("photo");
            imgPhoto.setImageBitmap(thumbnailBitmap);
        }
        //photo
        imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasPermissions()) {
                    makePhoto();
                } else {
                    requestPermissionWithRationale();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCall:
                if (hasPermissions()) {
                    if (!(editPhone.getText().toString().isEmpty())) {
                        if (Patterns.PHONE.matcher(editPhone.getText().toString()).matches()) {     //  validate a phone number
                            Dialog.dialogCall(this, phone);
                        } else {
                            ErrorDialog.dialogCallNotValid(this);
                        }
                    } else {
                        ErrorDialog.dialogCallIsEmpty(this);
                    }
                } else {
                    requestPermissionWithRationale();
                }
                break;
            case R.id.btnEmail:
                if (!(editEmail.getText().toString().isEmpty())) {
                    if (android.util.Patterns.EMAIL_ADDRESS.matcher(editEmail.getText().toString()).matches()) {  //   validate an e-mail address
                        Dialog.dialogEmail(this, email);  //  sendEmail();
                    } else {
                        ErrorDialog.dialogEmailNotValid(this);
                    }
                } else {
                    ErrorDialog.dialogEmailIsEmpty(this);
                }
                break;
            case R.id.btnSave:
                Dialog.dialogSave(this, nameFile, name, phone, email);
                break;
            case R.id.btnLoad:
                util.loadFromFile();
                thumbnailBitmap = Util.loadPhoto(util.photoPath);
                imgPhoto.setImageBitmap(thumbnailBitmap);
                editName.setText(util.name);
                editPhone.setText(util.phone);
                editEmail.setText(util.email);
                dataSave();
                break;
        }
    }

    private void makePhoto() {
        if (editable) {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
            //imgPhoto.setImageBitmap(Util.loadPhoto(nameFile));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            Util.makeCall(this, phone);
            return;
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            // Фотка сделана, извлекаем картинку
            thumbnailBitmap = (Bitmap) data.getExtras().get("data");
            Util.savePhoto(thumbnailBitmap, nameFile);
            Toast.makeText(this, "photo saved", Toast.LENGTH_SHORT).show();
            imgPhoto.setImageBitmap(thumbnailBitmap);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //ToolBar на панель приложения
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menuItemEdit = menu.findItem(R.id.edit_item);
        menuItemSave = menu.findItem(R.id.save_item);
        return super.onCreateOptionsMenu(menu);
    }

    // Edit -> Save
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_item:
                menuItemEdit.setVisible(false);
                menuItemSave.setVisible(true);
                editTextOpen();
                return true;
            case R.id.save_item:
                menuItemEdit.setVisible(true);
                menuItemSave.setVisible(false);
                dataSave();
                editTextClose();
                return true;
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    private void dataSave() {
        email = editEmail.getText().toString();
        name = editName.getText().toString();
        phone = editPhone.getText().toString();
    }

    public void editTextClose() {
        btnEmail.setVisibility(View.VISIBLE);
        btnEmail.setClickable(true);
        btnSave.setVisibility(View.VISIBLE);
        btnSave.setClickable(true);
        btnLoad.setVisibility(View.VISIBLE);
        btnLoad.setClickable(true);
        btnCall.setVisibility(View.VISIBLE);
        btnCall.setClickable(true);
        editable = false;
        imgPhoto.setClickable(false);
        editName.setEnabled(false);
        editEmail.setEnabled(false);
        editPhone.setEnabled(false);
    }

    public void editTextOpen() {
        btnEmail.setVisibility(View.INVISIBLE);
        btnEmail.setClickable(false);
        btnSave.setVisibility(View.INVISIBLE);
        btnSave.setClickable(false);
        btnLoad.setVisibility(View.INVISIBLE);
        btnLoad.setClickable(false);
        btnCall.setVisibility(View.INVISIBLE);
        btnCall.setClickable(false);
        editable = true;
        imgPhoto.setClickable(true);
        editName.setEnabled(true);
        editEmail.setEnabled(true);
        editPhone.setEnabled(true);
    }

    //состояние перемнных сохраняется в этом методе при Destroy()
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putParcelable("photo", thumbnailBitmap);
    }

    //permissions//permissions//permissions//permissions//permissions//permissions//permissions//permissions//permissions//permissions//permissions
    private boolean hasPermissions() {
        int res = 0;
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        for (String perms : permissions) {
            res = checkCallingOrSelfPermission(perms);
            if (!(res == PackageManager.PERMISSION_GRANTED)) {
                //если разрешение отсутствует
                return false;
            }
        }
        //разрешение есть
        return true;
    }

    private void requestPerms() {
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //после вызова этого метода появляется диалоговое окно с запросом разрешения
            requestPermissions(permissions, PERMISSION_REQUEST_CODE);
        }
    }

    //сюда приходит ответ пользователя на requestPermissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allowed = true;

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                //в grantResults[] --получено ли разрешение[i]
                for (int res : grantResults) {
                    // if user granted all permissions.
                    allowed = allowed && (res == PackageManager.PERMISSION_GRANTED);
                }
                break;
            default:
                // if user not granted permissions.
                allowed = false;
                break;
        }

        if (allowed) {
            //user granted all permissions we can perform our task.
            Util.makeCall(this, phone);
        } else {
            // we will give warning to user that they haven't granted permissions.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //отказывался ли ранее пользователь предоставлять разрешение
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Storage Permissions denied.", Toast.LENGTH_SHORT).show();
                } else {
                    showNoStoragePermissionSnackbar();
                }
            }
        }
    }


    //если был отказ больше одного раза
    public void showNoStoragePermissionSnackbar() {
        Snackbar.make(MainActivity.this.findViewById(R.id.activity_view), "Storage permission isn't granted", Snackbar.LENGTH_LONG)
                .setAction("SETTINGS", new View.OnClickListener() {
                    //в снекбаре кнопка, ведущая в настройки приложения
                    @Override
                    public void onClick(View v) {
                        openApplicationSettings();

                        Toast.makeText(getApplicationContext(),
                                "Open Permissions and grant the Storage permission",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                })
                .show();
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE);
    }

    //описывается причина запроса, если пользователь не дал разрешение
    public void requestPermissionWithRationale() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            final String message = "Storage permission is needed to show files count";
            Snackbar.make(MainActivity.this.findViewById(R.id.activity_view), message, Snackbar.LENGTH_LONG)
                    .setAction("GRANT", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestPerms();
                        }
                    })
                    .show();
        } else {
            requestPerms();
        }
    }
}
