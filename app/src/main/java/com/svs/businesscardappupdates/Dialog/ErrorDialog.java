package com.svs.businesscardappupdates.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ErrorDialog {
    public static void dialogCallNotValid(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder( context);
        builder.setMessage("Phone number is not valid")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       return;
                    }
                })
                .setCancelable(false)
                .show();
    }
    public static void dialogCallIsEmpty(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder( context);
        builder.setMessage("Phone field must be not empty")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        return;
                    }
                })
                .setCancelable(false)
                .show();
    }

    public static void dialogEmailNotValid(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder( context);
        builder.setMessage("Email is not valid")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        return;
                    }
                })
                .setCancelable(false)
                .show();
    }
    public static void dialogEmailIsEmpty(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder( context);
        builder.setMessage("Email field must be not empty")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        return;
                    }
                })
                .setCancelable(false)
                .show();
    }


}
