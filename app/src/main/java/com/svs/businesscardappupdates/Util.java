package com.svs.businesscardappupdates;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;


public class Util {
    String photoPath;
    String name;
    String phone;
    String email;


    private static Context context;
    private static final String FILE_NAME = "/data.txt";

    public static void saveToFile(Context context, final String photoPath, String name, String phone, String email) {
        JSONObject json = new JSONObject();
        try {
            json.put("photoPath", photoPath);
            json.put("name", name);
            json.put("phone", phone);
            json.put("email", email);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        try (FileWriter file = new FileWriter(new File(Environment.getExternalStorageDirectory().toString() + FILE_NAME))) {
            file.write(json.toString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(context, "Save to file", Toast.LENGTH_SHORT).show();
    }

    public static void makeCall(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }

    public static void sendEmail(Context context, String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + email));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public void loadFromFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory().toString() + FILE_NAME)))) {
            String json = reader.readLine();
            JSONObject obj = new JSONObject(json);
            photoPath = obj.getString("photoPath");
            name = obj.getString("name");
            phone = obj.getString("phone");
            email = obj.getString("email");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void savePhoto(Bitmap thumbnailBitmap, String nameFile) {
        OutputStream fOut = null;
        try {
            File file = new File(Environment.getExternalStorageDirectory().toString(), nameFile);
            fOut = new FileOutputStream(file);
            thumbnailBitmap.compress(Bitmap.CompressFormat.JPEG, 95, fOut);// сохранять картинку в jpeg-формате с 95% сжатия.
            fOut.flush();
            fOut.close();
            // MediaStore.Images.Media.insertImage(context.getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap loadPhoto(String nameFile) {
        File imgFile = new File(Environment.getExternalStorageDirectory().toString(), nameFile);
        Bitmap myBitmap = null;
        if (imgFile.exists()) {
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        return myBitmap;
    }

}
